import asyncio

from awg_driver import AWG
from vsg_driver import VSG

apollo = AWG("PXI10::CHASSIS2::SLOT1::INDEX0::INSTR", False, False)
artemis = VSG("TCPIP0::192.168.1.205::inst0::INSTR", "Artemis")


def create_modulated_sin_wave(I_file, Q_file, VSG_freq, VSG_amp):
    """

    Args:
        I_file: The csv file containing the wavepoints describing the cos wave of the desired additional frequency.
        Q_file: The csv file containing the wavepoints describing the sin wave of the desired additional frequency.
        VSG_freq: Set the VSG frequency.
        VSG_amp: Set the VSG amplitude.

    Returns:

    """
    apollo.make_a_waveform(I_file, "Channel1")
    apollo.make_a_waveform(Q_file, "Channel2")
    asyncio.run(artemis.set_frequency(VSG_freq))
    asyncio.run(artemis.set_power(VSG_amp))
    artemis.on()
    input("Press enter when you want to stop sending the signal.")
    artemis.off()
    apollo.device.Close()

I_file = r"C:\Users\LABadmin\Documents\Apollo\waveformfactory\waveformfiles\simple_cos.csv"
Q_file = r"C:\Users\LABadmin\Documents\Apollo\waveformfactory\waveformfiles\simple_sine.csv"
create_modulated_sin_wave(I_file, Q_file, 10e6, 9)
