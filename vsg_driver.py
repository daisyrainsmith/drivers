import pyvisa as visa
import logging
import os  # needed to check the working directory
import asyncio

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# # Replace the VISA address shown here with the VISA address of your instrument.
# # You'll find the VISA address within the IO Libraries installed on your PC.
# VISA_ADDRESS = "GPIB0::19::INSTR"

class VSG:
    def __init__(self, visa_address=None, name=None):
        print("attempt to initialize KeysightE8267D driver")
        # if no address then acts as a dummy simulation
        if name==None:
            self.name = "VSG: KeysightE8267D"
        else:
            self.name = name
        self.power = None
        self.frequency = None
        self.status = None
        if visa_address is None:
            self.simulation = True
        else:
            self.simulation = False
            self._name = visa_address
            self.init(visa_address)

    def init(self, visa_address):
        # Directory path to visa32.dll so it can be used by pyVISA
        rm = visa.ResourceManager('C:\\Windows\\System32\\visa32.dll')  # this uses pyvisa
        ## Open session using rm, which is the resource manager, and the visa address.
        self._mw = rm.open_resource(visa_address)
        ## Set Global Timeout
        self._mw.timeout = 1000
        ## Clear the instrument bus
        self._mw.clear()


#     ##############################################################################################################################################################################
#     ##############################################################################################################################################################################
#     ## FUNCTIONS TO INTERACT WITH THE DEVICE
#     ##############################################################################################################################################################################
#     ##############################################################################################################################################################################

    async def close(self):
        '''
        Close connection to the device

        Input:
            None

        Output:
            None
        '''
        if (self.simulation is False):
            self._mw.close()
        else:
            print("I am a dummy simulation")

    async def reset(self):
        '''
        Resets the instrument to default values

        Input:
            None

        Output:
            None
        '''
        logger.debug('resetting KeysightE8267D ' + self._name + ' instrument')
        self._mw.write('*RST')
        self.get_all()

    async def get_all(self):
        '''
        Reads all implemented parameters from the instrument,
        and updates the wrapper.

        Input:
            None

        Output:
            None
        '''
        logger.info(self.name + " " + self._name + ' : get all')
        await self.get_power()
        await self.get_frequency()
        await self.get_status()

    async def get_power(self):
        '''
        Reads the power of the signal from the instrument

        Input:
            None

        Output:
            ampl (float) : power in dBm
        '''
        logger.info(self.name + " " + self._name + ' : get power')
        self.power = float(self._mw.query('POW:AMPL?').rstrip('\n'))
        logger.info("Power = {}".format(self.power))
        return self.power

    async def set_power(self, amp):
        '''
        Set the power of the signal

        Input:
            amp (float) : power in dBm

        Output:
            None
        '''
        if amp>10:
            raise ValueError('VSG amplitude must be <=10dBm')
        logger.info(self.name + " " + self._name + ': set power to %g' % amp)
        self.power = amp
        self._mw.write('POW:AMPL %g' % amp)

    async def get_frequency(self):
        '''
        Reads the frequency of the signal from the instrument

        Input:
            None

        Output:
            freq (float) : Frequency in Hz
        '''
        logger.info(self.name + " " + self._name + ' : get frequency')
        self.frequency = float(self._mw.query('FREQ:CW?'))
        logger.info("Frequency = {}".format(self.frequency))
        return float(self.frequency)

    async def set_frequency(self, freq):
        '''
        Set the frequency rate of the signal from the instrument

        Input:
            freq (float) : Frequency in Hz

        Output:
            None
        '''
        logging.info(self.name + self._name + ': set frequency to %g' % freq)
        self.frequency = freq
        self._mw.write('FREQ:CW %g' % freq)

    async def off(self):
        '''
        Set status to 'off'

        Input:
            None

        Output:
            None
        '''
        logging.info("Turning off.")
        await self.set_status('off')

    async def on(self):
        '''
        Set status to 'on'

        Input:
            None

        Output:
            None
        '''
        logging.info("Turning on.")
        await self.set_status('on')

    async def get_status(self):
        '''
        Reads the output status from the instrument

        Input:
            None

        Output:
            status (string) : 'on' or 'off'
        '''
        logger.info(self.name + " " + self._name + ' : get status')
        stat = self._mw.query('OUTP?').strip()

        if (stat == '1'):
            self.status = 'on'
            logger.info("Status = " + self.status)
            return 'on'
        elif (stat == '0'):
            self.status = 'off'
            logger.info("Status = " + self.status)
            return 'off'
        else:
            logger.info("Status = " + self.status)
            raise ValueError('Output status not specified : %s' % stat)
        return

    async def set_status(self, status):
        '''
        Set the output status of the instrument

        Input:
            status (string) : 'on' or 'off'

        Output:
            None
        '''
        logging.info(self.name + " " + self._name + ' : set status to %s' % status)
        if status.upper() in ('ON', 'OFF'):
            status = status.upper()
        else:
            raise ValueError('set_status(): can only set on or off')
        self._mw.write('OUTP %s' % status)

if __name__ == '__main__':
    artemis = VSG("TCPIP0::192.168.1.205::inst0::INSTR", "Artemis")
    asyncio.run(artemis.get_all())
