import asyncio
import numpy as np
import matplotlib.pyplot as plt
import struct
import csv

def pack_waveform(data, marker):
    '''
    This function turns array of floats into array
    of Int32[] bytes, with 'markers' taking up the
    first 16 bits and the number described by the
    next 16 bits.

    data (array of floats)
    marker (array of ints)
    Not sure if we need this anymore. You can pass
    an array of floats between 1 and -1 into into
    apollo.load_waveform_array()
    '''
    marker_8bit = np.array(marker, dtype=np.ubyte)
    data_16bit = np.array(data * 32767, dtype=np.short)

    N = len(data)
    BBh = bytearray(N * 4)  # preallocate a memory block
    for i in range(N):
        BBh[4 * i:4 * i + 4] = struct.pack('<BBh', 0, marker_8bit[i], data_16bit[i])
        # < is for Little Endian, B is an unsigned byte,
        # h is a signed short (16bit) integer, making 32bits in total per sample
    return BBh

def Multitone(offsets, amplitudes, requested_duration=10e-6, srate=1.0e9, plots=False):
    '''
    Inputs (defaults):
        offsets - a list of IF tone frequencies, relative to the carrier
        amplitudes - a corresponding list of tone amplitudes (should be <=1)
        duration - total pulse duration in seconds (10us)
        srate - sample rate in Hz (1GHz)
        plots - boolean to show plots of the waveforms or not (False)
    Outputs a list of 3 arrays:
        time array in seconds
        I array of floats in [-1,1]
        Q array of floats in [-1,1]
    '''
    # speriod = 1 / srate --> (duration/speriod = duration * srate)
    requested_nsamples = requested_duration * srate
    # Make nsamples a multiple of 4, AWG only accepts this
    nsamples = 4 * (np.ceil(requested_nsamples/4))
    duration = float(nsamples) / srate
    time_full = np.linspace(0.0, duration, int(nsamples), endpoint=False)
    IQ_data = [0 + 0j] * len(time_full)  # complex zeros
    for ampl, offs in zip(amplitudes, offsets):
        IQ_data += ampl / np.sqrt(2) * np.cos(2 * np.pi * offs * time_full) + ampl / np.sqrt(2) * (0 + 1j) * np.sin(
            2 * np.pi * offs * time_full);  # its complex, but not complicated
        if offs == 0:
            raise ValueError(
                'You have chosen a zero offset which will be at the carrier frequency... thats not ideal...')
        if np.abs(offs) > srate or np.abs(offs) < (1 / duration):
            raise ValueError(
                'Your chosen IF offset frequency %4gHz seems not possible given your pulse duration and sample rate' % offs)

    if plots:
        # fourier transform for a frequncy domain plot
        n = len(time_full)
        Fy = np.fft.fft(IQ_data, n)
        Fx = np.fft.fftfreq(n, 1 / srate)

        plt.figure(420)
        plt.close()
        f, axarr = plt.subplots(2, num=420)

        axarr[0].plot(Fx, np.abs(Fy), 'b-')
        axarr[0].plot(Fx, np.abs(Fy), 'b.')
        axarr[0].set_title('Multitone pulse content')
        axarr[0].set_xlabel('frequency (Hz)')
        span = 1.2 * max(abs(np.array(offsets)))
        axarr[0].set_xlim([-span, span])

        axarr[1].plot(time_full, IQ_data.real, 'g-', time_full, IQ_data.imag, 'y-')
        axarr[1].legend(['In-phase', 'Quadrature'], framealpha=0.7)
        axarr[1].set_xlabel('time (s)')

        plt.ion()
        plt.show()
    return [time_full, IQ_data.real, IQ_data.imag]

def YbHyperfineFreqs(B):
    '''Calculate the microwave and RF resonance frequencies at a small B field value (in T) based on linear and quadratic Zeeman terms,
    returns a list: [MW-, MW0, MW+, RF-, RF+] in Hz'''

    HF = 12.642812118466e9 #clock splitting
    d2z = 31.080e9*(B**2) /2 #single state shift
    d1z = 28.024951e9*B /2 #single state shift

    return np.array([HF-d1z+d2z , HF+2*d2z, HF+d1z+d2z, d1z-d2z, d1z+d2z])

def create_configurable_multitone(AWG_instance, VSG_instance=None, VSG_freq=12.63e9, VSG_amp=10, ):
    """
    Args:
        AWG_instance: An AWG object.
        VSG_instance: A VSG object.
        VSG_freq: Set the VSG frequency, in Hz.
        VSG_amp: Set the VSG amplitude, in dBm.

    Returns:
    """
    freqs = YbHyperfineFreqs(1.2e-3)
    offsets = freqs[0:3] - VSG_freq

    _,I_data,Q_data = Multitone(offsets, [0.3,0.3,0.3], requested_duration=27.0e-6, srate=1.0e9)

    AWG_instance.load_waveform_array(I_data, "Channel1", "I_wave")
    AWG_instance.load_waveform_array(Q_data, "Channel2", "Q_wave")
    AWG_instance.initiate_output_2_channels("Channel1", "Channel2")
    input("Press enter when you want to stop signal.")
    AWG_instance.abort_generation("Channel1")
    AWG_instance.abort_generation("Channel2")
    #asyncio.run(artemis.set_frequency(VSG_freq))
    #asyncio.run(artemis.set_power(VSG_amp))
    #artemis.on()

freqs = YbHyperfineFreqs(1.2e-3)
offsets = freqs[0:3] - 12.64e9
_, I_data, Q_data = Multitone(offsets, [0.3, 0.3, 0.3], requested_duration=1.0e-6, srate=1.0e9)

def write_waveform_to_file(file_name, wave_data):
    with open(file_name, 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)

        for i in wave_data:
            writer.writerow([i])

I_data = pack_waveform(I_data, np.zeros_like(I_data))
#file_path = r"C:\Users\LABadmin\Documents\Apollo\waveformfactory\waveformfiles\test_file1.csv"
#write_waveform_to_file(file_path, I_data)

#apollo = AWG("PXI10::CHASSIS1::SLOT1::INDEX0::INSTR", False, False)
#artemis = VSG("TCPIP0::192.168.1.205::inst0::INSTR", "Artemis")

#apollo.load_waveform_array(I_data, channel="Channel3", waveform_name="Elizabeth")
#apollo.device.GetWaveform()

#input("Press enter when you want to stop sending the signal.")
#apollo.device.Close()

def test_2():
    return "test"

