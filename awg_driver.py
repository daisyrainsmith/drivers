import clr
import sys
import time
clr.AddReference("System")
from System import *
sys.path.append(r"C:\\Program Files\\IVI Foundation\\IVI\\Microsoft.NET\\Framework64\\v2.0.50727\\IviFoundationSharedComponents 1.4.1")
sys.path.append(r"C:\\Program Files\\IVI Foundation\\IVI\\Microsoft.NET\\Framework64\\v4.5.50709\\Keysight.KtMAwg 2.2.741")
sys.path.append(r"C:\Program Files\IVI Foundation\IVI\Microsoft.NET\Framework64\v4.5.50709\Keysight.KtMAwg 2.2.741\Source")
sys.path.append(r"C:\Program Files\Keysight\MAwg\bin")
clr.AddReference(r"Ivi.Driver")
clr.AddReference(r"Keysight.KtMAwg.Fx45")
clr.AddReference(r'C:\Program Files\Keysight\MAwg\bin\KtMAwgFundamental')
from Keysight.KtMAwg import KtMAwg, KtMAwgWaveformFactory
from Keysight.KtMAwg import IKtMAwgOutput, IKtMAwgArbitrary, IKtMAwgArbitraryWaveform
import os
from Keysight.KtMAwg import OutputMode
from Ivi.Fgen import IviFgen
from Ivi.Driver import IviDriver
import logging
import numpy as np

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

### 1 = burst waveform, 0 = continuous

#TODO:
#Separate loading waveforms and running waveforms into different functions
#Make function to list currently loaded waveforms

class AWG():
    """AWG Driver.
    Attributes:
        resource_name (str): An IVI logical name or an instrument specific string
                            that identifies the address of the instrument, such as
                            a VISA resource descriptor string.
        id_query (bool):    Specifies whether to verify the ID of the instrument.
        reset (bool):       Specifies whether to reset the instrument.

    """
    def __init__(self, resource_name=None, id_query=False, reset=False):
        self.resource_name=resource_name
        self.id_query = id_query
        self.reset = reset
        try:
            self.device = KtMAwg(self.resource_name, self.id_query, self.reset)
            self.arb_interface = IKtMAwgArbitrary(self.device)
            self.arb_waveform_interface = IKtMAwgArbitraryWaveform(self.device)
            self.output_interface = IKtMAwgOutput(self.device)
        except:
            raise RuntimeError("API failed to instantiate Apollo (AWG) object")
        logging.info("Apollo is initialised.")

    def close(self):
        self.device.Close()

    def set_sample_rate(self, channel="Channel3", rate=1.0e9):
        """
        Args:
            rate: (float) the sample rate (in Sa/s or Hz).
            channel: (String) e.g. Channel of the AWG that you want to send this waveform along.

        Returns:
            None

        """
        self.arb_interface.SetSampleRate(channel, rate)
        logging.info("Set sample rate to {} s-1".format(rate))

    def get_sample_rate(self, channel):
        return self.arb_interface.GetSampleRate(channel)

    def initiate_output(self, channel_string):
        self.device.InitiateGeneration(channel_string)
        logging.info("Initiating waveform on " + channel_string)

    def abort_output(self, channel_string):
        self.device.AbortGeneration(channel_string)
        logging.info("Aborting waveform on " + channel_string)

    def get_generation_state(self, channel):
        self.device.GetGenerationState(channel)

    def get_waveform(self, wave_name):
        return self.arb_waveform_interface.GetWaveform(wave_name)

    def get_waveform_info(self, wave_name):
        wave = self.get_waveform(wave_name)
        return "Wave name: {}, Channels: {}, Handle: {}, Length: {}".format(wave.Name, wave.Channels, wave.Handle, wave.Length)

    def load_waveform_file(self, file_path, channel="Channel3", waveform_name="UnnamedWaveform", samples=256):
        """
        Args:
            file_path: (String) Path to your waveform file.
            channel: (String) e.g. Channel of the AWG that you want to send this waveform along.
            waveform_name: Name of the type of waveform you are sending.
            samples: The number of samples in the file

        Returns:
            None
        """
        self.output_interface.SetEnabled(channel, True)
        logging.info("Set " + channel + " to enabled.")
        file_stream = KtMAwgWaveformFactory.OpenFile(file_path)
        handle = self.arb_waveform_interface.CreateChannelWaveform(channel, waveform_name, samples, file_stream)
        self.arb_waveform_interface.SetHandle(channel, handle)
        logging.info("Loaded waveform: " + waveform_name + " to " + channel)

    def load_waveform_array(self, wave_data, channel="Channel3", waveform_name="Unnamedwaveform"):
        """
        Args:
            wave_data: (Float[]) An array of floats between -1 and 1.
            channel: (String) e.g. Channel of the AWG that you want to send this waveform along.
            waveform_name: (String) Name of the type of waveform you are sending.

        Returns:
            None
        """
        wave_data = Array[Int16](wave_data*32767)
        self.output_interface.SetEnabled(channel, True)
        logging.info("Set " + channel + " to enabled.")
        handle = self.arb_waveform_interface.CreateChannelWaveform(channel, waveform_name, wave_data)
        self.arb_waveform_interface.SetHandle(channel, handle)
        logging.info("Loaded waveform: " + waveform_name + " to " + channel)
def test():
    return "test"

# def main():
#     apollo = AWG("PXI10::CHASSIS2::SLOT1::INDEX0::INSTR", False, False)
#     file_path=r"C:\Users\LABadmin\Documents\Apollo\waveformfactory\waveformfiles\test_file1.csv"
#     apollo.load_waveform_file(file_path, samples=1000)
#     time.sleep(10)
#     apollo.get_waveform()
#     input("Press enter when you want to stop.")
#     apollo.device.Close()
#
# if __name__ == "__main__":
#     main()