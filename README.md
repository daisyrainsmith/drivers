# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Operating the AWG and VSG

### How do I get set up? ###

Current experimental configuration:

AWG---------------------VSG---------------------Oscilloscope 				<br>
											
Ch1+ ------------------>I-input 											<br>
Ch1- ---> Nothing															<br>
Ch2+ ------------------>Q-input 											<br>
Ch2- ---> Nothing															<br>
Ch3+ -------------------------------------------------> Ch1 				<br>
Ch3- ---> Nothing															<br>
----------------------------Output -------------------> Ch4					<br>

If you want to _increase_ VSG frequency by 10MHz: <br>
I input: cos(10x) <br>
If you want to _decrease_ VSG frequency by 10MHz: <br>
Q input: sin(10x) <br>

### How to add methods to the AWG driver: ###

The Keysight manual is very confusing and it's difficult to 
work out wtf the interfaces are and stuff. So here is how you
access methods from the IVI.NET driver to add into the AWG driver.

The programming guide can be found here: <br>
C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Keysight 
Instrument Drivers\IVI.NET Drivers\KtMAwg 2.2.741.NET <br>
In the Programming Guide under: KtMAwg IVI.NET Driver/Reference 
you can find 'Driver Hierarchy' and 'Driver Interfaces'. These 
list the same functions that you can perform with the AWG. The 
Driver Hierarchy structure is useful to find the functions you 
want BUT WILL NOT HELP YOU USE THE FUNCTIONS. The structure does
not reflect how you import functions and use them. To do this you
need to use Driver Interfaces.


e.g. I want to use function: GetWaveform, which is located in the 
hierarchy under: IKtMAwg/Arbitrary/Waveform. However to import it 
into the driver I have to find the function in an interface. In 
this case it's IKtMAwgArbitraryWaveform.

So to use the function I have to:

1) import clr <br> clr.AddReference(r"Keysight.KtMAwg.Fx45") 
   <br> from Keysight.KtMAwg import IKtMAwgArbitraryWaveform
   
2) Create an instance of awg: <br>
apollo = AWG("PXI10::CHASSIS1::SLOT1::INDEX0::INSTR", False, False)
   
3) Create an instance of an IKtMAwgArbitraryWaveform interface. <br>
arb_waveform_interface = IKtMAwgArbitraryWaveform(apollo.device)
   
4) Use this interface to access the function: <br>
apollo.arb_waveform_interface.GetWaveform("a wave")

### Who do I talk to? ###

Daisy Smith - ds572@sussex.ac.uk
Eros Achillea
Sam Hile

